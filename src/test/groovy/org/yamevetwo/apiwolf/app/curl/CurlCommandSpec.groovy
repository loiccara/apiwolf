package org.yamevetwo.apiwolf.app.curl

import spock.lang.Specification

import static org.springframework.http.HttpMethod.GET

class CurlCommandSpec extends Specification {

    def "should get a full URL from a CurlCommand"(){
        given:
        CurlCommand curlCommand = new CurlCommand(
                httpMethod: GET,
                headers:  [
                        new CurlHeader(name: "first", value: "firstData"),
                        new CurlHeader(name: "last", value: "lastData")
                ],
                url:"grails.org",
                path: "/business",
                curlRequestParameters: [
                        new CurlRequestParameter(name: "first", value: "some%20data"),
                        new CurlRequestParameter(name: "second", value: "some%20other%20data")
                ]
        )

        expect:
        curlCommand.fullUrl ==  "grails.org/business?first=some%20data&second=some%20other%20data"
    }
}
