package org.yamevetwo.apiwolf.app.curl

import grails.test.mixin.TestFor
import org.yamevetwo.apiwolf.app.Action
import spock.lang.Specification

import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpMethod.GET

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(CurlService)
class CurlServiceSpec extends Specification {

    void "should build a curl from an empty CurlCommand"() {
        given:
        CurlCommand curlCommand = new CurlCommand()

        when:
        String curl = service.buildCurl(curlCommand)

        then:
        curl == "curl -i -X null ''"
    }

    void "should build a curl with an httpMethod"(){
        given:
        CurlCommand curlCommand = new CurlCommand(httpMethod: GET)

        when:
        String curl = service.buildCurl(curlCommand)

        then:
        curl == "curl -i -X GET ''"
    }

    void "should build a curl command with headers"(){
        given:
        List<CurlHeader> headers = [
                new CurlHeader(name:"first", value: "firstData"),
                new CurlHeader(name:"last", value:"lastData")
        ]
        CurlCommand curlCommand = new CurlCommand(httpMethod: GET, headers: headers)

        when:
        String curl = service.buildCurl(curlCommand)

        then:
        curl == "curl -i -X GET -H 'first: firstData' -H 'last: lastData' ''"
    }

    void "should build a curl command with a URL"(){
        CurlCommand curlCommand = new CurlCommand(httpMethod: GET, url: "grails.org")

        when:
        String curl = service.buildCurl(curlCommand)

        then:
        curl == "curl -i -X GET 'grails.org'"
    }

    void "should build a curl with request parameters"(){
        given:
        CurlCommand curlCommand = new CurlCommand(
                httpMethod: GET,
                curlRequestParameters: [
                        new CurlRequestParameter(name:"first", value: "some%20data"),
                        new CurlRequestParameter(name:"second", value:"some%20other%20data")
                ]
        )

        when:
        String curl = service.buildCurl(curlCommand)

        then:
        curl == "curl -i -X GET '?first=some%20data&second=some%20other%20data'"

    }

    void "should build a curl with path variable"(){
        given:
        CurlCommand curlCommand = new CurlCommand(
                httpMethod: GET,
                curlRequestParameters: [
                        new CurlRequestParameter(name:"first", value: "some%20data"),
                        new CurlRequestParameter(name:"second", value:"some%20other%20data")
                ]
        )

        when:
        String curl = service.buildCurl(curlCommand)

        then:
        curl == "curl -i -X GET '?first=some%20data&second=some%20other%20data'"
    }

    void "should build a curl with a full CurlCommand"() {
        given:
        List<CurlHeader> headers = [
                new CurlHeader(name: "first", value: "firstData"),
                new CurlHeader(name: "last", value: "lastData")
        ]
        CurlCommand curlCommand = new CurlCommand(
                httpMethod: GET,
                headers: headers,
                url:"grails.org",
                path: "/business",
                curlRequestParameters: [
                        new CurlRequestParameter(name: "first", value: "some%20data"),
                        new CurlRequestParameter(name: "second", value: "some%20other%20data")
                ]
        )

        when:
        String curl = service.buildCurl(curlCommand)

        then:
        curl == "curl -i -X GET -H 'first: firstData' -H 'last: lastData' 'grails.org/business?first=some%20data&second=some%20other%20data'"
    }
}
