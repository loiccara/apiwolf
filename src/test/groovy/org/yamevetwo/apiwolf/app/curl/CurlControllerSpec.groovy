package org.yamevetwo.apiwolf.app.curl

import grails.plugins.rest.client.RestResponse
import grails.test.mixin.TestFor
import groovy.json.JsonSlurper
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import spock.lang.Specification

import static org.springframework.http.HttpStatus.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(CurlController)
class CurlControllerSpec extends Specification {

    def curlServiceMock

    def setup(){
        curlServiceMock = Mock(CurlService)
        controller.curlService = curlServiceMock
    }

    def "should build a curl for a given curlCommand"() {
        given:
        request.method == 'GET'
        CurlCommand curlCommand = new CurlCommand()

        when:
        controller.buildCurl(curlCommand)

        then:
        1 * curlServiceMock.buildCurl(curlCommand) >> "some curl"
        println response.toString()
        response.getJson() == [curl:"some curl"]
    }

    def "should execute a given curl command"(){
        given:
        request.method == 'POST'
        CurlCommand curlCommand = new CurlCommand()

        when:
        controller.executeCurl(curlCommand)

        then:
        1 * curlServiceMock.executeCurl(curlCommand) >> new RestResponse()
        response.getStatus() == OK.value()
    }
}
