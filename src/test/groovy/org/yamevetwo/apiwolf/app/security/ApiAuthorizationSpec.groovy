package org.yamevetwo.apiwolf.app.security

import grails.test.mixin.TestFor
import org.yamevetwo.apiwolf.app.FieldType
import org.yamevetwo.apiwolf.app.security.ApiAuthorization
import spock.lang.Specification

@TestFor(ApiAuthorization)
class ApiAuthorizationSpec extends Specification {

    void "should execute a return command"() {
        given:
        ApiAuthorization apiAuthorization = new ApiAuthorization(
                name: "dummy authorization",
                fieldCode: "authorization",
                fieldType: FieldType.HEADER,
                algorithm: "return auth.fieldCode"
        )

        expect:
        apiAuthorization.evalAlgorithm() == apiAuthorization.fieldCode
    }

}
