package org.yamevetwo.apiwolf.app.security

import grails.test.mixin.TestFor
import spock.lang.Specification

import java.security.MessageDigest

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(EncryptService)
class EncryptServiceSpec extends Specification {

    void "test something"() {
        given:
        String password = "pass"

        when:
        MessageDigest sha1 = MessageDigest.getInstance("SHA1")
        byte[] digest  = sha1.digest(password.getBytes())
        System.out.println(new  BigInteger(1, digest).toString(16))

        then:
        true
    }
}
