package org.yamevetwo.apiwolf.app.curl

/**
 * Created by loic on 06/06/16.
 */
class CurlHeader {

    static final String SEPARATOR=":"

    String name
    String value
    boolean isPartOfSignature
}
