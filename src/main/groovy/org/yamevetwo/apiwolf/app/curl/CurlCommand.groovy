package org.yamevetwo.apiwolf.app.curl

import grails.validation.Validateable
import groovy.transform.ToString
import org.springframework.http.HttpMethod
import org.yamevetwo.apiwolf.app.Action

@ToString(includePackage = false)
class CurlCommand implements Validateable{

    HttpMethod httpMethod
    String url
    String path
    String body
    String secretKey

    List<CurlHeader> headers
    List<CurlRequestParameter> curlRequestParameters
    List<CurlRequestParameter> pathVariables

    static constraints = {
        url nullable: false
        path nullable:false
        body nullable:true
        secretKey nullable:true
    }

    String getFullUrl() {
        def urlBuilder = StringBuilder.newInstance()
        urlBuilder << (url ?: "")
        urlBuilder << (path ?: "")
        urlBuilder << getActionAndParamsUrl()
        urlBuilder.toString()
    }

    private String getActionAndParamsUrl() {
        def urlBuilder = StringBuilder.newInstance()
        urlBuilder << getPathVariablesAsString()
        urlBuilder << (curlRequestParameters ? "?" : "")
        urlBuilder << curlRequestParameters
                .findAll{it.value != null}
                .collect{"${it.name}${CurlRequestParameter.SEPARATOR}${it.value}"}
                .join("&")

        urlBuilder.toString()
    }

    private String getPathVariablesAsString(){
        (pathVariables?.size()>0)?"/" + pathVariables.collect{it.value}.join('/') + "/" :""
    }
}