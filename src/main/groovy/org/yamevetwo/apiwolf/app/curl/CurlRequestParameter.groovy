package org.yamevetwo.apiwolf.app.curl

class CurlRequestParameter {

    static final String SEPARATOR="="

    String name
    String value
    boolean isPartOfSignature
}
