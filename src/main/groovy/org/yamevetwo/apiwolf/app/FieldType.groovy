package org.yamevetwo.apiwolf.app

enum FieldType {
    HEADER(":", "headers"),
    BODY("", ""),
    REQUEST_PARAMETER("=", "curlRequestParameters"),
    PATH_VARIABLE("/", "pathVariables")

    String separator
    String collectionName

    FieldType(String separator, String collectionName){
        this.separator = separator
        this.collectionName = collectionName
    }
}
