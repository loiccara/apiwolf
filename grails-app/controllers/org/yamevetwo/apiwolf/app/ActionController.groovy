package org.yamevetwo.apiwolf.app

class ActionController {

    static allowedMethods = [getActionTemplate:'GET']

    def getActionTemplate() {
        Action action = Action.get(params.id)
        render(template: "/templates/action", model: [action: action])
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Action.list(params), model:[actionCount: Action.count()]
    }

    def edit(Action action){
        respond action
    }
}
