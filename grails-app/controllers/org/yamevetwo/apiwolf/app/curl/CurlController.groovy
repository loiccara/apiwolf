package org.yamevetwo.apiwolf.app.curl

import grails.converters.JSON
import grails.plugins.rest.client.RestResponse
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

class CurlController {

    static responseFormats = ['json']

    def curlService

    def executeCurl(CurlCommand curlCommand) {
        RestResponse resp = curlService.executeCurl(curlCommand)
        respond JSON.parse(new JsonBuilder(resp).toString())
    }

    def buildCurl(CurlCommand curlCommand){
        def jsonSlurper = new JsonSlurper()
        String curl = curlService.buildCurl(curlCommand)
        def object = jsonSlurper.parseText """
        {"curl":"${curl}"}
        """
        respond object
    }
}
