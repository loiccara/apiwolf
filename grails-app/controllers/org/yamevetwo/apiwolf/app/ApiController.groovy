package org.yamevetwo.apiwolf.app

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

@Transactional
class ApiController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", addEnvironment: "POST"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Api.list(params), model:[apiCount: Api.count()]
    }

    def show(Api api) {
        respond api
    }

    def create() {
        respond new Api(params)
    }

    def edit(Api api){
        respond api
    }

    @Transactional
    def save(Api api) {
        if (!api.validate()) {
            respond api.errors, view: 'create'
            return
        }

        api.save flush: true, failOnError: true

        flash.message = message(code: 'default.created.message', args: [message(code: 'api.label', default: 'Api'), api.name])
        redirect api
    }

    @Transactional
    def update(Api api) {
        if (!api.validate()) {
            respond api.errors, view: 'edit'
            return
        }

        api.save flush: true

        flash.message = message(code: 'default.updated.message', args: [message(code: 'api.label', default: 'Api'), api.name])
        redirect action: 'edit', id:api.id
    }

    @Transactional
    def delete(Api api) {
        if (!api) {
            notFound()
            return
        }

        api.delete flush:true, failOnError:true

        flash.message = message(code: 'default.deleted.message', args: [message(code: 'api.label', default: 'Api'), api.name])
        redirect action:"index"
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'api.label', default: 'Api'), api.name.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Transactional
    def addEnvironment(Long id){
        Api api = Api.get(id)

        if (!api) {
            notFound()
            return
        }

        Environment env = new Environment(api:api)
        api.environments.add(env)
        api.save(flush:true, failOnError:true)

        render(template:'/templates/edit/environments', model:[api:api])
    }

    @Transactional
    def deleteEnvironment(Long id){
        Environment environment = Environment.get(id)
        Api api = environment.api

        if (!environment) {
            notFound()
            return
        }

        environment.delete(flush:true)

        render(template:'/templates/edit/environments', model:[api:api])
    }

    @Transactional
    def addAction(Long id){
        Api api = Api.get(id)

        if (!api) {
            notFound()
            return
        }

        Action action = new Action(api:api)
        api.actions.add(action)
        api.save(flush:true, failOnError:true)

        render(template:'/templates/edit/actions', model:[api:api])
    }

    @Transactional
    def deleteAction(Long id){
        Action action = Action.get(id)

        if (!action) {
            notFound()
            return
        }

        Api api = action.api
        action.delete(flush:true)

        render(template:'/templates/edit/actions', model:[api:api])
    }

    @Transactional
    def addField(Long id){
        Action action = Action.get(id)

        if (!action) {
            notFound()
            return
        }

        ApiField apiField = new ApiField(action:action, defaultValues: [])
        action.apiFields.add(apiField)
        action.save(flush:true, failOnError:true)

        render(template:'/templates/edit/apiFields', model:[action:action])
    }

    @Transactional
    def deleteField(Long id){
        ApiField apiField = ApiField.get(id)

        if (!apiField) {
            notFound()
            return
        }

        Action action = apiField.action
        apiField.delete(flush:true)

        render(template:'/templates/edit/apiFields', model:[action:action])
    }
}
