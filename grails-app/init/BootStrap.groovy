import org.yamevetwo.apiwolf.app.Action
import org.yamevetwo.apiwolf.app.Api
import org.yamevetwo.apiwolf.app.ApiField
import org.yamevetwo.apiwolf.app.Environment

import static org.springframework.http.HttpMethod.*
import static org.yamevetwo.apiwolf.app.FieldType.*

class BootStrap {

    def init = { servletContext ->

        new Api(
                name: "Weather Api",
                shortName: "Weather",
                fontAwesomeIcons: ["sun-o", "bolt"]
        )
                .addToEnvironments(new Environment(name: "LIVE", url: "http://api.openweathermap.org/data/2.5"))
                .addToActions(
                new Action(
                        name: "search",
                        httpMethod: GET,
                        url: "/weather"
                ).addToApiFields(
                        new ApiField(
                                name: "Location",
                                fieldCode: "q",
                                fieldType: REQUEST_PARAMETER
                        ).addToDefaultValues("London")
                ).addToApiFields(
                        new ApiField(
                                name: "Api key",
                                fieldCode: "APPID",
                                fieldType: REQUEST_PARAMETER
                        ).addToDefaultValues("ca6775c0d54fa3b1d08f328f96833e84")
                )
        ).save()
    }
    def destroy = {
    }
}
