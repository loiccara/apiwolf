<!doctype html>
<html lang="en" class="no-js">
<head>
    <asset:stylesheet src="bootstrap.min.css"/>
    <asset:stylesheet src="custom.css"/>
    <asset:stylesheet src="font-awesome.min.css"/>
    <asset:javascript src="jquery-1.12.4.min.js" />
    <asset:javascript src="bootstrap.min.js" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>ApiWolf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <style type="text/css">
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
    </style>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <script type="text/javascript">
        window.contextPath = "${request.contextPath}";
    </script>
</head>
<body>
<g:render template="/templates/menu"/>
<g:layoutBody/>
<g:render template="/templates/footer"/>

<asset:javascript src="/test/api/util/dateFormat.js"/>
<asset:javascript src="/test/api/util/cryptojs/rollups/hmac-sha256.js" />
<asset:javascript src="/test/api/util/cryptojs/components/enc-base64-min.js" />
<asset:javascript src="/test/api/util/clipboard.js" />
<asset:javascript src="/test/api/test.api.js"/>
<asset:javascript src="/test/api/index/services/validationGateway.js"/>
<asset:javascript src="showApi.js" />
<asset:javascript src="index.js" />

<asset:javascript src="cryptojs/rollups/hmac-sha256.js" />
<asset:javascript src="cryptojs/components/enc-base64-min.js" />
<asset:javascript src="clipboard.js" />

</body>