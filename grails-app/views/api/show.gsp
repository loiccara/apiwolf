<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'api.label', default: 'Api')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>

    </head>
    <body>
        <div class="container">
            <div class="page-header" id="banner">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h1>${api.name}</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div>
                        <form id="curlCommand" class="form">
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Environment</label>
                                    <select name="curlCommand.url" id="environmentChoice" class="form-control" onchange="updateEnvironment()">
                                        <option value="" disabled selected>Select your option</option>
                                        <g:each in="${this.api.environments}" var="env">
                                            <option value="${env.url}">${env.name} ${env.url}</option>
                                        </g:each>
                                    </select>
                                </div>
                                <div class="col-xs-6" id="actionChoice" style="display: none">
                                    <label>Action</label>
                                    <select class="form-control" onchange="updateAction(this.value)">
                                        <option value="" disabled selected>Select your option</option>
                                        <g:each in="${this.api.actions}" var="action">
                                            <option value="${action.id}">${action.name} ${action.url} ${action.httpMethod}</option>
                                        </g:each>
                                    </select>
                                </div>
                            </div>

                            <div class="row" id="action" style="display: none">

                            </div>

                            <div class="row" id="curl" style="display: none">
                                <div class="col-xs-12">
                                    <h3>Curl</h3>
                                    <textarea id="copyTarget" rows="5" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="row" id="buttons" style="display: none">
                                <g:render template="/templates/buttons" />
                            </div>

                            <div class="row" id="curlResult" style="display:none">
                                <hr class="col-xs-12">
                                <h3>Curl result</h3>
                                <div class="col-xs-12">
                                    <label>Status:</label>
                                    <input id="status" type="text" class="form-control"/>
                                    <label>Result headers:</label>
                                    <textarea id="headers" rows="10" class="form-control"></textarea>
                                    <label>Result body:</label>
                                    <textarea id="responseBody" rows="20" class="form-control"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
