<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'api.label', default: 'Api')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="row"><div class="col-sm-12">
                                <table class="table table-striped table-hover dataTable no-footer" id="dataTables-example"
                                       role="grid" aria-describedby="dataTables-example_info">
                                    <thead>
                                    <tr role="row">
                                        <th>Api name</th>
                                        <th>Short name</th>
                                        <td>Icons</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <g:each in="${apiList}" var="api" status="i">
                                        <tr>
                                            <td><g:link action="edit" id="${api.id}">${api.name}</g:link></td>
                                            <td>${api.shortName}</td>
                                            <td>
                                                <g:each in="${api.fontAwesomeIcons}">
                                                    <i class="fa fa-${it}"></i>
                                                </g:each>
                                            </td>
                                            <td>
                                                <g:form resource="${api}" method="DELETE">
                                                    <button type="submit" class="btn btn-primary"
                                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i
                                                            class="fa fa-times"></i>
                                                    </button>
                                                </g:form>
                                            </td>
                                        </tr>
                                    </g:each>
                                    </tbody>
                                </table>
                            </div></div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Actions
                </div>

                <div class="panel-body">
                    <g:link class="create" action="create"><button type="button" class="btn btn-primary">
                        New Api
                    </button></g:link>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
    </body>
</html>