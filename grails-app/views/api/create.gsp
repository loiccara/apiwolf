<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'api.label', default: 'Api')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Create 192 User</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <g:render template="/templates/validationErrors" model="['objectWithErrors': oneNineTwoUser]"/>

        <g:form action="save">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            192 User
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="username">Name <span class="required-indicator">*</span></label>
                                    <input name="username" type="text" class="form-control" value="${oneNineTwoUser.username}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="password">Password <span class="required-indicator">*</span></label>
                                    <input name="password" type="text" class="form-control" value="${oneNineTwoUser.password}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="hasAdminRight">Is Admin</label>
                                    <input name="hasAdminRight" type="checkbox" class="form-control" value="true" checked>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="hasLogsRight">Can see logs</label>
                                    <input name="hasLogsRight" type="checkbox" class="form-control" value="false">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Actions
                        </div>
                        <div class="panel-body">
                            <input class="btn btn-primary" type="submit" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                        </div>
                    </div>
                </div>
            </div>
        </g:form>


    
        <a href="#create-api" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="create-api" class="content scaffold-create" role="main">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.api}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.api}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form action="save">
                <fieldset class="form">
                    <f:all bean="api"/>
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
