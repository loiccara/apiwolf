<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'api.label', default: 'Api')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<script>
    var count = 10;
    function appendFontawesomeIcon() {
        count++;
        var id = "font-"+count
        console.log(count);
        $("#fontIcons")
                .append('<div class="row" id="'+id+'">' +
                        '   <div class="col-lg-10"><input name="fontAwesomeIcons" type="text" class="form-control" value=""></div>' +
                        '   <div class="col-lg-2"><p class="btn btn-primary" onclick="$(\'#'+id+'\').remove();"><i class="fa fa-minus"></i></p></div>'+
                        '</div>');
    }

</script>
<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <h1 class="page-header">Edit Api</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<g:form resource="${this.api}" method="PUT">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">

            <g:render template="/templates/conditionalMessage"/>
            <g:render template="/templates/validationErrors" model="['objectWithErrors': api]"/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Api Data
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="name">Name <span class="required-indicator">*</span></label>
                                <input name="name" type="text" class="form-control" value="${api.name}"
                                       required>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="shortName">Short name <span class="required-indicator">*</span></label>
                                <input name="shortName" type="text" class="form-control" value="${api.shortName}"
                                       required>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Icons</label>
                                <div id="fontIcons">
                                <g:each in="${api.fontAwesomeIcons}" var="icon" status="i">
                                    <div class="row" id="font-${i}">
                                        <div class="col-lg-10"><input name="fontAwesomeIcons" type="text" class="form-control" value="${icon}"></div>
                                        <div class="col-lg-2"><p class="btn btn-primary" onclick="$('#font-${i}').remove();"><i class="fa fa-minus"></i></p></div>
                                    </div>
                                </g:each>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2"><p class="btn btn-primary" onclick="appendFontawesomeIcon()"><i class="fa fa-plus"></i> Add icon</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p>Environmnents</p>
                        </div>
                        <div class="panel-body">
                            <div id="environments" >
                                <g:render template="/templates/edit/environments" model="[api:api]"/>
                            </div>

                            <div class="col-lg-2">
                                <p class="btn btn-primary" onclick="addEnvironment(${this.api.id})"><i class="fa fa-plus"></i> Add Environment</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="actions">
                <g:render template="/templates/edit/actions" model="[api:api]"/>
            </div>

            <div class="panel panel-default"><div class="panel-body">
                <div class="col-lg-2">
                    <p class="btn btn-primary" onclick="addAction(${this.api.id})"><i class="fa fa-plus"></i> Add Action</p></div>
                </div>
            </div>

        </div>



        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Actions
                </div>

                <div class="panel-body">
                    <input class="btn btn-primary" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                </div>
            </div>
        </div>
    </div>
    </g:form>
</body>

</html>
