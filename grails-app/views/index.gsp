<%@ page import="org.yamevetwo.apiwolf.app.Api" %>
<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta name="layout" content="main" />
</head>
<body>
<div class="container">
    <div class="page-header" id="banner">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <p>Welcome to the 192 APIs test tool. Please choose an API to test.</p>
            </div>
            <div class="col-md-5">
                <ul class="nav navbar-nav navbar-right" onclick="showOnlyTiles()">
                    <li><a href="#"><span class="fa fa-th-large" aria-hidden="true"/></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right" onclick="showOnlyList()">
                    <li><a href="#"><span class="fa fa-list" aria-hidden="true"/></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div>
        <div class="row" id="apisList" style="display: none">
            <div class="col-md-10 col-md-offset-1">

                <ul>
                    <g:each in="${org.yamevetwo.apiwolf.app.Api.findAll()}" var="api">
                        <li><p><g:link controller="Api" action="show" id="${api.id}">${api.name}</g:link></p></li>
                    </g:each>
                </ul>
            </div>
        </div>

        <div class="row" id="apisTiles">
            <div class="col-md-10 col-md-offset-1">
                <g:each in="${org.yamevetwo.apiwolf.app.Api.findAll()}" var="api">
                    <div class="col-sm-6 col-md-3">
                        <g:link controller="Api" action="show" id="${api.id}">
                            <div class="thumbnail tile tile-medium tile-green">
                                <h3>${api.shortName}</h3>
                                <g:each in="${api.fontAwesomeIcons}">
                                    <i class="fa fa-3x fa-${it}"></i>
                                </g:each>
                            </div>
                        </g:link>
                    </div>
                </g:each>
            </div>
        </div>
    </div>
</div>
</body>
</html>
