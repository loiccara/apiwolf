<div id="action${action.httpMethod}">
    <input type="hidden" name="curlCommand.httpMethod" value="${action.httpMethod}">
    <input type="hidden" name="curlCommand.path" value="${action.url}">
    <hr class="col-xs-12">
    <g:if test="${action.body}">
        <div class="col-xs-6">
    </g:if>
    <g:else>
        <div class="col-xs-12">
    </g:else>
        <g:if test="${action.headers.size() > 0}">
            <h3>Headers</h3>
            <g:each in="${action.headers}" var="header" status="index">
                <g:render template="/templates/apiField" model="['apiField':header, index:index]" />
            </g:each>
        </g:if>

        <g:if test="${action.pathVariables.size() > 0}">
            <h3>Path Variables</h3>
            <g:each in="${action.pathVariables}" var="pathVariable" status="index">
                <g:render template="/templates/apiField" model="['apiField':pathVariable, index:index]" />
            </g:each>
            <hr class="col-xs-12">
        </g:if>

        <g:if test="${action.requestParameters.size() > 0}">
            <h3>Request Parameters</h3>
            <g:each in="${action.requestParameters}" var="requestParameter" status="index">
                <g:render template="/templates/apiField" model="['apiField':requestParameter, index:index]" />
            </g:each>
            <hr class="col-xs-12">
        </g:if>
    </div>

    <g:if test="${action.body}">
        <g:javascript type="javascript">
            function updateBody(data){
                $("#body").val(data)
            }
        </g:javascript>
        <div class="col-xs-12">
            <h3>Body</h3>

            <label>Default Json:</label>
            <select class="form-control" onchange="updateBody(this.value)">
                <option value="" disabled selected>Select</option>
                <g:each in="${action.body?.defaultValues}" var="defaultValue" status="i">
                    <option value="${defaultValue}">default ${i}</option>
                </g:each>
            </select>

            <label>JSON data:</label>
            <textarea id="body" name="curlCommand.body" rows="20" class="form-control"></textarea>
        </div>
    </g:if>
</div>