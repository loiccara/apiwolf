<g:if test="${flash.message}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Info
                </div>
                <div class="panel-body">
                    <div class="message">${flash.message}</div>
                </div>
            </div>
        </div>
    </div>
</g:if>

<g:if test="${flash.error}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    Info
                </div>
                <div class="panel-body">
                    <div class="message">${flash.error}</div>
                </div>
            </div>
        </div>
    </div>
</g:if>