<footer class="page-footer">
    <div class="container">
        <div class="row">
            <hr>

            <div class="col-md-10 col-md-offset-1">
                <p>Test tool using Grails 3.X </p>
            </div>
        </div>
    </div>
</footer>

<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>