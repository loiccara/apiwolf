<div class="col-xs-3">
    <br>
    <button type="button" class="btn btn-primary" onclick="callBuildCurl()">
        <i class="fa fa-refresh" aria-hidden="true"></i>
        Refresh CURL
    </button>
</div>
<div class="col-xs-3">
    <br>
    <button id="copyButton" type="button" class="btn btn-primary">
        <i class="fa fa-clipboard" aria-hidden="true"></i>
        Copy
    </button>
</div>
<div class="col-xs-3">
    <br>
    <button id="play" type="button" class="btn btn-primary" onclick="callApi()">
        <i class="fa fa-play" aria-hidden="true"></i>
        Play
    </button>
</div>
<div class="col-xs-3">
    <br>
    <button type="button" class="btn btn-primary">
        <i class="fa fa-upload" aria-hidden="true"></i>
        Upload
    </button>
</div>