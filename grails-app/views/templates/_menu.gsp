<%@ page import="org.yamevetwo.apiwolf.app.Api" %>
<div class="container">
    <div id="dropdownMenu">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <g:link url="/" class="navbar-brand">Apiwolf</g:link>
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-main">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">APIs  <span
                                    class="caret"/></a>
                            <ul class="dropdown-menu" aria-labelledby="themes">
                                <g:each in="${org.yamevetwo.apiwolf.app.Api.findAll()}" var="api">
                                    <li><g:link controller="Api" action="show" id="${api.id}">${api.name}</g:link></li>
                                </g:each>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="./help.html" target="_blank">Help</a></li>
                        <li><g:link controller="Api"><i class="fa fa-cogs fa-fw"></i></g:link></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>