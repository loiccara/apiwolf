<g:each in="${api.environments}" var="environment" status="i">
    <div id="env-${i}">
        <div class="row">
            <div class="col-lg-1">
                <p class="btn btn-primary" onclick="deleteEnvironment(${environment.id})"><i class="fa fa-minus"></i></p>
            </div>
            <div class="col-lg-5">
                <div class="form-group">
                    <input name="environments[${i}].name" type="text" placeholder="Name" class="form-control" value="${environment.name}">
                    <input name="environments[${i}].id" type="hidden" class="form-control" value="${environment.id}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input name="environments[${i}].url" type="text" placeholder="Url" class="form-control" value="${environment.url}">
                </div>
            </div>
        </div>
    </div>
</g:each>