<g:each in="${action.apiFields}" var="field" status="j">
    <div class="row">
        <div class="col-lg-1">
            <div class="form-group">
                <p class="btn btn-primary" onclick="deleteField(${field.id}, ${action.id})"><i class="fa fa-minus"></i></p> <label>Field ${j+1}</label>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <input name="actions[${status}].apiFields[${j}].fieldCode" type="text" placeholder="Code" class="form-control" value="${field.fieldCode}"/>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <input name="actions[${status}].apiFields[${j}].name" type="text" placeholder="Name" class="form-control" value="${field.name}"/>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <input name="actions[${status}].apiFields[${j}].fieldType" type="text" placeholder="Type" class="form-control" value="${field.fieldType}"/>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-11 col-lg-offset-1">
            <p>Default Values</p>
        </div>
    </div>
    <div class="row">
        <g:each in="${field.defaultValues}" var="defaultValue">
            <div class="col-lg-11 col-lg-offset-1">
                <div class="form-group">
                    <input name="actions[${status}].apiFields[${j}].defaultValues" type="text" placeholder="Default Value" class="form-control" value="${defaultValue}"/>
                </div>
            </div>
        </g:each>
    </div>
</g:each>