<g:each in="${api.actions}" var="action" status="i">
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="btn btn-primary" onclick="deleteAction(${action.id})"><i class="fa fa-minus"></i></p> Action ${i + 1}
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input name="actions[${i}].name" type="text" placeholder="Name" class="form-control" value="${action.name}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input name="actions[${i}].httpMethod" type="text" placeholder="HttpMethod" class="form-control" value="${action.httpMethod}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <input name="actions[${i}].url" type="text" placeholder="Url" class="form-control" value="${action.url}">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div id="apiFields_${action.id}">
                            <g:render template="/templates/edit/apiFields" model="[action:action, status:i]"/>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <p class="btn btn-primary" onclick="addField(${action.id})"><i class="fa fa-plus"></i> Add Field</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</g:each>