<label>${apiField?.name}</label>
<input type="hidden" name="curlCommand.${apiField.fieldType.collectionName}[${index}].name" class="form-control" value="${apiField?.fieldCode}"/>
<g:if test="${apiField.defaultValues.size() > 1}">
    <select name="curlCommand.${apiField.fieldType.collectionName}[${index}].value" class="form-control">
        <option value="" disabled selected>Select</option>
        <g:each in="${apiField.defaultValues}">
            <option value="${it}">${it}</option>
        </g:each>
    </select>
</g:if>
<g:else>
    <input type="text"  name="curlCommand.${apiField.fieldType.collectionName}[${index}].value" class="form-control" value="${apiField.defaultValues?(apiField.defaultValues.get(0)?:''):''}" placeholder="${apiField?.name}" onchange="callBuildCurl()">
</g:else>
