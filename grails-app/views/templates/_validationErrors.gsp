<g:hasErrors bean="${objectWithErrors}">
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                Errors
            </div>
            <div class="panel-body">
                <ul class="errors" role="alert">
                    <g:eachError bean="${objectWithErrors}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>>
                            <g:message error="${error}"/>
                        </li>
                    </g:eachError>
                </ul>
            </div>
        </div>
    </div>
</div>
</g:hasErrors>
