package org.yamevetwo.apiwolf.app.curl

import grails.plugins.rest.client.RequestCustomizer
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.transaction.Transactional
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.ResourceAccessException

@Transactional
class CurlService {

    static final String CURL_PREFIX = "curl -i "

    def curls = [
            GET: { CurlCommand curlCommand ->
                new RestBuilder().get(curlCommand.fullUrl) {
                    curlCommand.headers.each { CurlHeader curlHeader ->
                        headers.add(curlHeader.name, curlHeader.value)
                    }
                }
            },
            POST: {CurlCommand curlCommand ->
                new RestBuilder().post(curlCommand.fullUrl) {
                    curlCommand.headers.each { CurlHeader curlHeader ->
                        headers.add(curlHeader.name, curlHeader.value)
                    }
                    body(curlCommand.body)
                }
            }
    ]

    def executeCurl(CurlCommand curlCommand) {
        log.debug("[executeCurl] With curlCommand: ${curlCommand.toString()}")
        String url = curlCommand.fullUrl

        RequestCustomizer requestCustomizer = new RequestCustomizer()
        curlCommand.headers.each { CurlHeader curlHeader ->
            requestCustomizer.header(curlHeader.name, curlHeader.value)
        }
        try {
            def resp = curls.get(curlCommand.httpMethod.toString())(curlCommand)
        } catch (ResourceAccessException e){
            new RestResponse(new ResponseEntity(e.message, HttpStatus.UNPROCESSABLE_ENTITY))
        }
    }

    String buildCurl(CurlCommand curlCommand) {
        log.debug("[buildCurl] with curlCommand: ${curlCommand.toString()}")
        def curl = StringBuilder.newInstance()
        curl << CURL_PREFIX
        curl << "-X ${curlCommand.httpMethod} "

        curlCommand.headers.each {CurlHeader header->
            curl << "-H '${header.name}${CurlHeader.SEPARATOR} ${header.value}' "
        }
        curl << ((curlCommand.body?/-d '${curlCommand.body}' /:"").replaceAll("\"", "\\\\\""))
        curl << "'${curlCommand.fullUrl}'"
        curl.toString()
    }
}
