package org.yamevetwo.apiwolf.app

class ApiField {

    String name
    String fieldCode
    FieldType fieldType
    List<String> defaultValues

    static hasMany = [defaultValues:String]

    static belongsTo = [action:Action]

    static constraints = {
        name nullable:true
        fieldCode nullable:true
        fieldType nullable:true
    }
}
