package org.yamevetwo.apiwolf.app

class Environment {

    String name
    String url

    static belongsTo = [api:Api]

    static constraints = {
        name nullable: true
        url nullable: true
    }
}
