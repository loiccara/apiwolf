package org.yamevetwo.apiwolf.app.security

import org.yamevetwo.apiwolf.app.Action
import org.yamevetwo.apiwolf.app.FieldType

class ApiAuthorization {

    String name
    String fieldCode
    FieldType fieldType
    String algorithm

    static belongsTo = [Action]

    static constraints = {
        name nullable: false
        fieldCode nullable: false
        fieldType nullable: false
        algorithm nullable:true
    }

    def evalAlgorithm(){
        def shell = new GroovyShell()
        def binding = new Binding(auth:this)
        def script = shell.parse(algorithm)
        script.binding = binding
        script.run()
    }
}
