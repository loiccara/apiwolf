package org.yamevetwo.apiwolf.app

import org.springframework.http.HttpMethod
import org.yamevetwo.apiwolf.app.security.ApiAuthorization

import static org.yamevetwo.apiwolf.app.FieldType.*

class Action {

    String name
    HttpMethod httpMethod
    List<ApiField> apiFields
    ApiAuthorization apiAuthorization
    String url

    static belongsTo = [api:Api]

    static hasMany = [apiFields:ApiField]

    static constraints = {
        name nullable: true
        httpMethod nullable: true
        apiFields nullable: true, blank:true
        apiAuthorization nullable: true
        url nullable:true
    }

    List<ApiField> getHeaders(){
        apiFields.findAll{ApiField field ->
            field.fieldType == HEADER
        }
    }

    List<ApiField> getRequestParameters(){
        apiFields.findAll{ApiField field ->
            field.fieldType == REQUEST_PARAMETER
        }
    }

    ApiField getBody(){
        apiFields.find{ApiField field ->
            field.fieldType == BODY
        }
    }

    List<ApiField> getPathVariables(){
        apiFields.findAll{ApiField field ->
            field.fieldType == PATH_VARIABLE
        }
    }
}
