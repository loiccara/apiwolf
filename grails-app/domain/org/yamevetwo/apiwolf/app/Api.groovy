package org.yamevetwo.apiwolf.app

class Api {

    String name

    // UI Data
    String shortName
    List<String> fontAwesomeIcons

    List<Environment> environments
    List<Action> actions

    static hasMany = [actions:Action, environments:Environment, fontAwesomeIcons:String]

    static constraints = {
    }
}
