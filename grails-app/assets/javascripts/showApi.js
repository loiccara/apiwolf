function updateEnvironment() {
    $( "#actionChoice" ).show();
}

function updateAction(actionId){
    var url = "http://localhost:9095/action/getActionTemplate/" + actionId
    $.ajax({
        url: url,
        type: 'GET',
        beforeSend: function(xhr) {

        }, success: function(data){
            $('#action').html(data);
            showAction();
        }
    })
}

function showAction(){
    $( "#action" ).show();
    $( "#buttons" ).show();
    $( "#security" ).show();
    callBuildCurl();
    $( "#curl" ).show();
    $( "#curlResult" ).hide();
}

$("input").on("change paste keydown keyup", function() {
    buildAuthorization();
    callBuildCurl();
});

function buildAuthorization() {
    var formValues=getFormValues();

    $.ajax({
        url: 'http://localhost:9095/curl/buildAuthorization',
        type: 'GET',
        data: formValues,
        beforeSend: function(xhr) {

        }, success: function(data){
            updateAuthorization(data);
        }
    })
}

function callBuildCurl(){
    var formValues=getFormValues();

    $.ajax({
        url: 'http://localhost:9095/curl/buildCurl',
        type: 'GET',
        data: formValues,
        beforeSend: function(xhr) {
            console.log(xhr);
        }, success: function(data){
            updateCurl(data);
        }
    })
}

function callApi() {
    var formValues = getFormValues();
    callBuildCurl();
    $.ajax({
        url: 'http://localhost:9095/curl/executeCurl',
        type: 'POST',
        data: formValues,
        beforeSend: function(xhr) {

        }, success: function(data){
            updateCurlResult(data);
        }
    })
}

function updateCurl(data){
    $( "#curl textarea" ).val(data.curl);
}

function updateCurlResult(data){
    var prettyHeaders = JSON.stringify(data.responseEntity.headers, undefined, 4);
    var prettyBody = JSON.stringify(data.responseEntity.body, undefined, 4);

    $( "#curlResult #status" ).val(""+data.status+" "+data.statusCode);
    $( "#curlResult #headers" ).text(prettyHeaders);
    $( "#curlResult #responseBody" ).text(data.responseEntity.body);
    $( "#curlResult" ).show();
}

function updateAuthorization(data){
    $( "input[placeholder~='Authorization']").val(data.auth);
}

function getFormValues(){
    var inputs = $('#curlCommand :input');

    var values = {};
    inputs.each(function() {
        values[this.name] = $(this).val();
    });

    return values;
}


function addEnvironment(apiId) {

    $.ajax({
        url: 'http://localhost:9095/api/addEnvironment/'+apiId,
        type: 'POST',
        beforeSend: function(xhr) {

        }, success: function(data){
            updateEnvironments(data);
        }
    })
}

function deleteEnvironment(envId){
    $.ajax({
        url: 'http://localhost:9095/api/deleteEnvironment/'+envId,
        type: 'POST',
        beforeSend: function(xhr) {

        }, success: function(data){
            updateEnvironments(data);
        }
    })
}

function updateEnvironments(data){
    $('#environments').html(data);
}

function addAction(apiId) {

    $.ajax({
        url: 'http://localhost:9095/api/addAction/'+apiId,
        type: 'POST',
        beforeSend: function(xhr) {

        }, success: function(data){
            updateActions(data);
        }
    })
}

function deleteAction(actionId){
    $.ajax({
        url: 'http://localhost:9095/api/deleteAction/'+actionId,
        type: 'POST',
        beforeSend: function(xhr) {

        }, success: function(data){
            updateActions(data);
        }
    })
}

function updateActions(data){
    $('#actions').html(data);
}

function addField(actionId) {
    console.log(actionId);
    $.ajax({
        url: 'http://localhost:9095/api/addField/'+actionId,
        type: 'POST',
        beforeSend: function(xhr) {

        }, success: function(data){
            updateFields(data, actionId);
        }
    })
}

function deleteField(fieldId, actionId){
    $.ajax({
        url: 'http://localhost:9095/api/deleteField/'+fieldId,
        type: 'POST',
        beforeSend: function(xhr) {

        }, success: function(data){
            updateFields(data, actionId);
        }
    })
}

function updateFields(data, id){

    console.log(data);
    var apiFieldsId = "apiFields_" + id;
    console.log(apiFieldsId);
    $("#"+apiFieldsId).html(data);
}






